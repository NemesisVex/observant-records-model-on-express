var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
    res.append('Access-Control-Allow-Origin', '*');
    res.append('Access-Control-Allow-Methods', 'GET');
    res.append('Access-Control-Allow-Headers', 'Origin, Content-Type, Accept, Authorization, X-Request-With, X-CLIENT-ID, X-CLIENT-SECRET');
    res.append('Access-Control-Allow-Credentials', 'true');
    res.json([
        { id: 1, lastName: 'Eponymous 4', displayName: 'Eponymous 4', alias: 'eponymous-4' },
        { id: 2, lastName: 'Empty Ensemble', displayName: 'Empty Ensemble', alias: 'empty-ensemble' },
        { id: 3, lastName: 'Penzias and Wilson', displayName: 'Penzias and Wilson', alias: 'penzias-and-wilson' }
    ]);
});

module.exports = router;
